<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls;

class CacheKeyException extends \Exception {}
