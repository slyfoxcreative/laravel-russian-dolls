<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls;

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function boot(): void
    {
        if (app()->isLocal()) {
            app(Kernel::class)->pushMiddleware(FlushViews::class);
        }

        \Blade::directive('cache', function ($expression) {
            return "<?php if (!app('SlyFoxCreative\\RussianDolls\\BladeDirective')->setUp({$expression})) : ?>";
        });

        \Blade::directive('endcache', function () {
            return "<?php endif; echo app('SlyFoxCreative\\RussianDolls\\BladeDirective')->tearDown() ?>";
        });
    }

    public function register()
    {
        app()->singleton(BladeDirective::class);
    }
}
